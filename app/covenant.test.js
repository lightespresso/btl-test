const { createStore, applyMiddleware } = require('redux')
const createSagaMiddleware = require('redux-saga').default
const SagaTester = require('redux-saga-tester').default
const axios = require('axios')

const {
  actionCreators,
  initialState,
  actionTypes,
  reducer,
  rootSaga,
  selectors
} = require('./covenant.js')

const MOCK_SUCCESS_RESPONSE = {
  timeZoneName: 'UTC',
  utcOffset: '00:00:00',
  isDayLightSavingsTime: false,
  currentDateTime: '2018-11-16T15:33Z',
  ordinalDate: '2018-320',
  dayOfTheWeek: 'Friday'
}

const MOCK_ERROR_RESPONSE = {
  serviceResponse: 'ERROR'
}

const resolves = data => async () => ({ data })
const wait = async () => {}

const setup = () => {
  app = new SagaTester({
    reducers: reducer,
    initialState
  })
  app.start(rootSaga)

  const select = selector => selector(app.getState())
  
  return { app, select }
}

describe('sagaTimezoneDetails', () => {
  it('should handle success response', async () => {
    const { app, select } = setup()
    
    jest
      .spyOn(axios, 'get')
      .mockImplementation(resolves(MOCK_SUCCESS_RESPONSE))
    
    app.dispatch(actionCreators.sagaTimezoneDetails())
  
    await app.waitFor(actionTypes.UPDATE_TIMEZONE_DETAILS)
  
    const {
      timeZoneName,
      utcOffset,
      isDayLightSavingsTime,
      currentDateTime,
      ordinalDate,
      dayofTheWeek
    } = MOCK_SUCCESS_RESPONSE
  
    const timezoneDetails = select(selectors.getTimezoneDetails)
    const timezoneError = select(selectors.getLatestErrorError)
    
    expect(timezoneError).toBe(undefined)
    expect(timezoneDetails).toEqual({
      timeZoneName,
      utcOffset,
      isDayLightSavingsTime,
      currentDateTime,
      ordinalDate,
      dayofTheWeek
    })
  })
  
  it('should handle failure response', async () => {
    const { app, select } = setup()
    jest
      .spyOn(axios, 'get')
      .mockImplementation(resolves(MOCK_ERROR_RESPONSE))
    
    app.dispatch(actionCreators.sagaTimezoneDetails())
  
    await app.waitFor(actionTypes.TIMEZONE_API_ERROR)
  
    const timezoneError = select(selectors.getLatestErrorError)
    
    expect(timezoneError).toMatchObject({
      error: MOCK_ERROR_RESPONSE.serviceResponse
    })
  })

  it('should handle app errors', async () => {
    const error = new Error()
    const { app, select } = setup()
    jest
      .spyOn(axios, 'get')
      .mockImplementation(() => {
        throw error
      })
    
    app.dispatch(actionCreators.sagaTimezoneDetails())

    await app.waitFor(actionTypes.TIMEZONE_API_ERROR)
  
    const timezoneError = select(selectors.getLatestErrorError)
    
    expect(timezoneError).toMatchObject({ error })
  })
})
